const express = require('express');
const router = express.Router();

User = require('../models/user');

//  User page routes
router.get('/users', function(req, res){
    User.find({})
        .exec()
        .then(function(products) {
            res.json(products);
        })
        .catch(function(err) {
            console.log("Error: " + err);
    })
});

router.post('/user', function(req, res){
    let user = new User(req.body);
    user.save()
        .then(function(product) {
            res.json(product);
        })
        .catch(function(err) {
            console.log('Error: ' + err.message);
        })
});

router.put('/user/:_id', function(req, res){
    let id = req.params._id;
    let user = req.body;
    let update = {
        firstName: req.body.firstName || res.firstName,
        lastName: req.body.lastName || res.lastName,
        money: req.body.money || res.money
    };

    User.findByIdAndUpdate(id, update)
        .exec()
        .then(function(user) {
            res.json(user);
        })
        .catch(function(err) {
            console.log('Error: ' + err.value);
        })
});

router.delete('/user/:_id', function(req, res){
    let id = req.params._id;
    User.remove({id})
        .exec()
        .then(function(user) {
            console.log('Removed: ' + id);
            res.json(user);
        })
        .catch(function(err) {
            console.log('Error: ' + err.value + ' is wrong ID');
        })
});

module.exports = router;