const express = require('express');
const router = express.Router();

Order = require('../models/order');
User = require('../models/user');
Product = require('../models/product');

//  Order page routes
router.get('/orders', function(req, res){
    Order.find({})
        .exec()
        .then(function(products) {
            res.json(products);
        })
        .catch(function(err) {
            console.log("Error: " + err);
    })
});


router.post('/order', function(req, res){
    //console.log(req.params);
    let productID = req.query.product;
    let orderQuant = req.query.quantity;
    let userID = req.query.user;

    Promise.all([
        User.findById(userID).select('money').exec(),
        Product.findById(productID).select('quantity price').exec()
    ])
    .then(values => {
        //console.log(values)
        let user = values[0];
        let product = values[1];
        let productQuant = values[1].quantity;
        let productPrice = values[1].price;
        console.log('User money: ', user.money);
        console.log('Product quant in shop: ', productQuant);
        console.log('Product price: ', productPrice);
        console.log('Order quantity ', orderQuant);
        
        // checking conditions
        if (orderQuant > productQuant) {
            console.log('Not enough product quantity in shop');
        } else if (user.money < productPrice * orderQuant) {
            console.log('Not enough users money for purchase');
        } else {
            
            // deduct money from user
            user.money = user.money - (product.price * orderQuant);
            user.save().then(function(user){
                console.log('User money updated!');
                //res.json(user); 
            })
            
            // update Product quantity
            product.quantity = productQuant - orderQuant;
            product.save().then(function(product){
                console.log('Product quantity updated!');
                //res.json(product); 
            })

            // Create record
            orderData = {
                user: userID,
                product: productID,
                quantity: orderQuant
            };
            let order = new Order(orderData);
            order.save()
                .then(function(order){
                    console.log('Order created!');
                    res.json(product);
                })
                .catch(function(err) {
                    console.log('Error: ' + err.message);
                })
        }
    })
    .catch(function(err) { console.log(err) })
});


module.exports = router;