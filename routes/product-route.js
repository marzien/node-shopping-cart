const express = require('express');
const router = express.Router();

Product = require('../models/product');

//  Product page routes
router.get('/products', function(req, res){
    Product.find({})
        .exec()
        .then(function(products) {
            res.json(products);
        })
        .catch(function(err) {
            console.log("error " + err);
        })
          
});

router.get('/product/:_id', function(req, res){
    Product.findById({_id:req.params._id})
        .exec()
        .then(function(product) {
            res.json(product);
        })
        .catch(function(err) {
            console.log('Error: ' + err.value + ' is wrong ID');
        })
});

router.post('/product/', function(req, res){
    let product = new Product(req.body);
    //console.log(Product.create(product))
    //Product.create(product)
    product.save()
        .then(function(product) {
            res.json(product);
        })
        .catch(function(err) {
            console.log('Error: ' + err.message);
        })
});

router.put('/product/:_id', function(req, res){
    let id = req.params._id;
    let product = req.body;
    let update = {
        // title: req.product.title || product.title,
        // type: req.product.type || product.type,
        // url: req.product.url || product.url,
        // quantity: req.product.quantity || product.quantity,
        // price: req.product.price || product.price
        title: req.body.title || res.title,
        type: req.body.type || res.type,
        url: req.body.url || res.url,
        quantity: req.body.quantity || res.quantity,
        price: req.body.price || res.price
    };

    Product.findByIdAndUpdate(id, update)
    .exec()
    .then(function(product) {
        res.json(product);
    })
    .catch(function(err) {
        console.log('Error: ' + err.value);
    })
});

router.delete('/product/:_id', function(req, res){
    let id = req.params._id;
    Product.remove({id})
        .exec()
        .then(function(product) {
        	console.log('Removed: ' + id);
            res.json(product);
        })
        .catch(function(err) {
            console.log('Error: ' + err.value + ' is wrong ID');
        })
});

module.exports = router;