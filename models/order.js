const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// User Schema
let orderSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},      //NEED User Ref to user by ID
    product: {type: Schema.Types.ObjectId, ref: 'Product'},   //NEED Product Ref to product by ID
    quantity: {type: Number, required: true},
    createdOn: {type: Date, default: Date.now}
});

let Order = module.exports = mongoose.model('Order', orderSchema);
