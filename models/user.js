const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// User Schema
let userSchema = new Schema({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    money: {type: Number, required: true}
});

let User = module.exports = mongoose.model('User', userSchema);
